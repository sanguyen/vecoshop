<?php $real_post = $post;
$ent_attrs = get_option('youtube_showcase_attr_list');
?>
<div class="emd-video-wrapper" style="display:inline-block;border:1px solid lightgray;border-radius:4px;width:100%;padding:3px;margin:5px;">
<div class="emd-embed-responsive">
	<iframe src="https://www.youtube.com/embed/<?php echo esc_html(emd_mb_meta('emd_video_key')); ?>
?html5=1" frameborder="0" allowfullscreen></iframe>
	</div>

<a title="<?php echo get_the_title(); ?>" href="<?php echo get_permalink(); ?>">
<div class="video-title widget" style="padding:0 5px"><?php echo get_the_title(); ?></div>
</a>
</div>