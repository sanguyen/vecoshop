jQuery( document ).ready( function( $ ) {
	//'use strict';

	// create namespace to avoid any possible conflicts
	$.select_product_unit_on_single_product_page = {
		init: function() {

			var select_unit = $("#pa_don-vi-mua");
			var size_table = $("table.size-attribute");
			// var variation_price = $("div.woocommerce-variation.single_variation");
   			variation_raw_selects = $("select#pa_size");

			if ( select_unit.val() == "ri_nho" || select_unit.val() == "ri_trung" || select_unit.val() == "ri_lon") {
				size_table.hide();
				// variation_price.hide();

				variation_raw_selects.each( function(index){
					$(this).data("previousSelectedIndex", $(this).prop('selectedIndex'));
					$(this).prop('selectedIndex', 1);
				});
			}

			select_unit.on("change", function(){
				if ( select_unit.val() == "ri_nho" || select_unit.val() == "ri_trung" || select_unit.val() == "ri_lon") {
					size_table.hide();
					// variation_price.hide();
					variation_raw_selects.each( function(index){
						$(this).data("previousSelectedIndex", $(this).prop('selectedIndex'));
						$(this).prop('selectedIndex', 1);
					});
				} else {
					size_table.show();
					// variation_price.show();
					variation_raw_selects.each( function(index){
						$(this).prop('selectedIndex', $(this).data("previousSelectedIndex"));
					});
				}
			});
		}
	}; // close namespace

	$.select_product_unit_on_single_product_page.init();

	// $( '.variations_form' ).on( 'show_variation', function( event, variation ) {
	// 	var select_unit = $("#pa_don-vi-mua");
	// 	var quantity = $("input.input-text.qty");
		
	// 	var qty_min_for_banle = $(".qty_min_for_banle").val();
	// 	var qty_min_for_ri = $(".qty_min_for_ri").val();

	// 	if ( select_unit.val() == "ri_nho" || select_unit.val() == "ri_trung" || select_unit.val() == "ri_lon") {
	// 		$(quantity).prop("min", qty_min_for_ri);
	// 		$(quantity).val(qty_min_for_ri);
	// 	} else{
	// 		$(quantity).prop("min", qty_min_for_banle);
	// 		$(quantity).val(qty_min_for_banle);
	// 	}
	// } );


});
