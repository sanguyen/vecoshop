<?php
 // var_dump(openssl_get_cert_locations()); die;

// echo "<pre>";
// print_r(openssl_get_cert_locations());
// echo "</pre>";

require_once get_stylesheet_directory() . '/shortcodes.php';
require_once get_stylesheet_directory() . '/my_account.php';
require_once get_stylesheet_directory() . '/inc/widgets/widget-recent-saleoff-posts.php';
require_once get_stylesheet_directory() . '/inc/widgets/widget-recent-blog-posts.php';

/**
* Declare global variable
*/
define('DON_VI_MUA', 'Đơn vị mua');
define('DON_VI_MUA_KEY', 'pa_don-vi-mua');

define('SIZE', 'Size');
define('SIZE_KEY', 'pa_size');

// define('RI', 'ri');
define('RI_NHO', 'ri nhỏ');
define('RI_TRUNG', 'ri trung');
define('RI_LON', 'ri lớn');
define('MIN_QUANTITY_LE', 5);
define('MIN_QUANTITY_RI', 1);
// array of product's units
$units = array(
     'cai'         => __( 'cái', 'vecoshop' ),
     'bo'         => __( 'bộ', 'vecoshop' ),
     'ri_nho'         => __( 'ri nhỏ', 'vecoshop' ),
     'ri_trung'         => __( 'ri trung', 'vecoshop' ),
     'ri_lon'         => __( 'ri lớn', 'vecoshop' ),
);

// array of ri's value
$ri_array = array(
    'ri_nho',
    'ri_trung', 
    'ri_lon'
);

$le_array = array(
    'cai',
    'bo'
);

// Add custom Theme Functions here

/***********************************************
Register the Tab 'Đơn vị tính của sản phẩm' by hooking into the 'woocommerce_product_data_tabs' filter
************************************************/

add_filter( 'woocommerce_product_data_tabs', 'add_unit_product_data_tab' );
function add_unit_product_data_tab( $product_data_tabs ) {
    $product_data_tabs['product-unit'] = array(
        'label' => __( 'Mô tả cho từng đơn vị tính', 'vecoshop' ),
        'target' => 'unit_product_data',
        'class'     => array( 'show_if_simple', 'show_if_variable'  ),
        'priority' => '65'
    );
    return $product_data_tabs;
}

add_action( 'woocommerce_product_data_panels', 'add_my_custom_product_data_fields' );
function add_my_custom_product_data_fields() {
    global $woocommerce, $post;

    ?>
    <!-- id below must match target registered in above add_my_custom_product_data_tab function -->
    <div id="unit_product_data" class="panel woocommerce_options_panel">
        <?php

            // $dvm_terms = wc_get_product_terms( $post->ID, DON_VI_MUA_KEY , array( 'fields' => 'all' ) );
            // foreach ( $dvm_terms as $dvm ) {
            //     $dvm_item_name = '_dvm_' . $dvm->slug;
            //     // $dvm_item_value =  get_term_meta( $dvm->term_id, $dvm_item_name, true );
            //     $dvm_item_value = get_post_meta( $post->ID, $dvm_item_name, true );

            //     woocommerce_wp_textarea_input( array(
            //         'id'          => $dvm_item_name,
            //         'value'       => $dvm_item_value,
            //         'style'       => 'width:100%;height:140px;',
            //         'label'       => __( 'Mô tả cho đơn vị "' . $dvm->name . '"', 'vecoshop' ),
            //         'description' => __( 'Mô tả này sẽ hiển thị ở trang chi tiết sản phẩm', 'vecoshop' ),
            //     ) );
            // }

            $dvm_terms = array(
                 'ri_nho'         => __( 'ri nhỏ', 'vecoshop' ),
                 'ri_trung'         => __( 'ri trung', 'vecoshop' ),
                 'ri_lon'         => __( 'ri lớn', 'vecoshop' ),
            );
            foreach ( $dvm_terms as $dvm_key=>$dvm_value ) {
                $dvm_item_name = '_dvm_' . $dvm_key;
                $dvm_item_value = get_post_meta( $post->ID, $dvm_item_name, true );

                woocommerce_wp_textarea_input( array(
                    'id'          => $dvm_item_name,
                    'value'       => $dvm_item_value,
                    'style'       => 'width:100%;height:140px;',
                    'label'       => __( 'Mô tả cho đơn vị "' . $dvm_value . '"', 'vecoshop' ),
                    'description' => __( 'Mô tả này sẽ hiển thị ở trang chi tiết sản phẩm', 'vecoshop' ),
                ) );
            }
 


        ?>
    </div>
    <?php
}

/**
* 'mynl2br' function helps get new line in textarea
*/
function mynl2br($text) { 
   return strtr($text, array("\r\n" => '<br />', "\r" => '<br />', "\n" => '<br />')); 
} 

/**
* Save all detail infor of product units
*/

function save_product_units_detail_info( $post_id ) {
    // $dvm_terms = wc_get_product_terms( $post_id, DON_VI_MUA_KEY, array( 'fields' => 'all' ) );
    // foreach ( $dvm_terms as $dvm ) {
    //     $dvm_item_name = '_dvm_' . $dvm->slug;
    //     $dvm_item_value = $_POST[$dvm_item_name];
    //     update_post_meta( $post_id, $dvm_item_name, $dvm_item_value );
    // }

    $dvm_terms = array(
         'ri_nho'         => __( 'ri nhỏ', 'vecoshop' ),
         'ri_trung'       => __( 'ri trung', 'vecoshop' ),
         'ri_lon'         => __( 'ri lớn', 'vecoshop' ),
    );

    // $dvm_terms = wc_get_product_terms( $post_id, DON_VI_MUA_KEY, array( 'fields' => 'all' ) );
    foreach ( $dvm_terms as $dvm_key=>$dvm_value ) {
        $dvm_item_name = '_dvm_' . $dvm_key;
        $dvm_item_value = $_POST[$dvm_item_name];
        update_post_meta( $post_id, $dvm_item_name, $dvm_item_value );
    }

}

add_action( 'woocommerce_process_product_meta', 'save_product_units_detail_info' );

/***************************************
Enqueue script for single product page
***************************************/
add_action( 'wp_enqueue_scripts', 'select_product_unit_enqueue_scripts' );
function select_product_unit_enqueue_scripts() {
    // if( is_singular( 'product') ) {
        wp_enqueue_script( 'select-product-unit', get_stylesheet_directory_uri() . '/assets/js/select-product-unit.js', array( 'jquery' ), '1.0', true );
     // }
}

// global $product;

// add_filter( 'woocommerce_variation_prices', 'remove_ri_price_from_variation_prices',10, 3 );
// function remove_ri_price_from_variation_prices( $prices, $product, $include_taxes) {
//     global $ri_array;

//     if ( $product->is_type( 'variable' ) ) {
//         $variation_ids  = $product->get_visible_children();
//         foreach ( $variation_ids as $variation_id ) {
//             $variation = wc_get_product( $variation_id );
//             $variation_attributes = $variation->get_attributes();
//             if( isset( $variation_attributes[DON_VI_MUA_KEY] ) && in_array( $variation_attributes[DON_VI_MUA_KEY], $ri_array ) ){
//                 if( isset( $prices['price'] ) ) {
//                     unset( $prices['price'][$variation_id] );
//                 }
//                 if( isset( $prices['regular_price'] ) ) {
//                     unset( $prices['regular_price'][$variation_id] );
//                 }
//                 if( isset( $prices['sale_price'] ) ) {
//                     unset( $prices['sale_price'][$variation_id] );
//                 }
//             }
//         }
//     }

//     return $prices;
// }

/**
* Get prices for each units
* return array of price_html for each product units 
*/
function get_each_product_unit_price_html( $product ){
    global $ri_array;
    $unit_price_html = array();
    
    $dvm_terms = wc_get_product_terms( $product->get_id(), DON_VI_MUA_KEY );
    foreach ( $dvm_terms as $dvm ) {
        // if ( in_array( $dvm->slug, $ri_array ) ) {
             $unit_price_html[ $dvm->slug ] = '';
        // }
    }
   
    if ( $product->is_type( 'variable' ) ) {
        $variation_ids  = $product->get_visible_children();
        foreach ( $variation_ids as $variation_id ) {
            $variation = wc_get_product( $variation_id );
            $variation_attributes = $variation->get_attributes();
            if( isset( $variation_attributes[DON_VI_MUA_KEY] ) ) {
                $ri_variation  = $product->get_available_variation( $variation_id );
                if( $ri_variation['display_regular_price'] == $ri_variation['display_price'] ) {
                    $unit_price_html[$variation_attributes[DON_VI_MUA_KEY]] = wc_price( $ri_variation['display_price'] );
                }
                else {
                    $unit_price_html[$variation_attributes[DON_VI_MUA_KEY]] = wc_format_sale_price($ri_variation['display_regular_price'], $ri_variation['display_price']);
                }
                // break;
            }
        }
    }
    return $unit_price_html;
}

/**
* Get the min product unit: 'cai' or 'bo'
*/

// function get_min_product_unit( $product ) {
//     $dvm_terms = wc_get_product_terms( $product->get_id(), DON_VI_MUA_KEY );
//     foreach ( $dvm_terms as $dvm ) {
//         if ( $dvm->slug == 'cai' || $dvm->slug == 'bo' ) {
//             return $dvm->slug;
//         }
//     }
//     return 'cai';
// }

/**
* Hide all attributes for RI in cart
*/
function hide_size_attributes_for_ri_in_cart( $item_data, $cart_item ) {
    global $ri_array;
    $hidden = false;

    foreach ( $item_data as $key => $data ) {
        if ( isset($data['key']) && $data['key'] == DON_VI_MUA && in_array( $data['value'], array_map( 'ucfirst', array( RI_NHO, RI_TRUNG, RI_LON ) ) ) ) {
            $hidden = true;
            break;
        }
    }
    if ( $hidden ) {
        foreach ( $item_data as $key => $data ) {
            // Set hidden to true to not display meta on cart.
            if ( $data['key'] == SIZE ) {
               $item_data[ $key ]['hidden'] = $hidden;
            }
        }
    }
    return $item_data;
}
add_filter('woocommerce_get_item_data', 'hide_size_attributes_for_ri_in_cart', 10, 2);

/**
* Hide all attributes for RI in order
*/
function hide_size_item_in_order_for_ri( $html, $item, $args  ) {
    global $ri_array;
    $strings = array();
    $html    = '';
    $args    = wp_parse_args( $args, array(
        'before'    => '<ul class="wc-item-meta"><li>',
        'after'     => '</li></ul>',
        'separator' => '</li><li>',
        'echo'      => true,
        'autop'     => false,
    ) );

    $is_ri = false;
    foreach ( $item->get_formatted_meta_data() as $meta_id => $meta ) {
        if ( $meta->key == DON_VI_MUA_KEY && in_array( $meta->value, $ri_array ) )  {
             $is_ri = true;
             break;
        }
    }

    foreach ( $item->get_formatted_meta_data() as $meta_id => $meta ) {
        if ( !$is_ri ) {
            $value = $args['autop'] ? wp_kses_post( $meta->display_value ) : wp_kses_post( make_clickable( trim( $meta->display_value ) ) );
            $strings[] = '<strong class="wc-item-meta-label">' . wp_kses_post( $meta->display_key ) . ':</strong> ' . $value;       
        }
        else {
            if ( $meta->key != SIZE_KEY ) {
                $value = $args['autop'] ? wp_kses_post( $meta->display_value ) : wp_kses_post( make_clickable( trim( $meta->display_value ) ) );
                $strings[] = '<strong class="wc-item-meta-label">' . wp_kses_post( $meta->display_key ) . ':</strong> ' . $value;  
            }
        }
    }

    if ( $strings ) {
        $html = $args['before'] . implode( $args['separator'], $strings ) . $args['after'];
    }

    if ( $args['echo'] ) {
        echo $html;
    } else {
        return $html;
    }
    
}
add_action('woocommerce_display_item_meta', 'hide_size_item_in_order_for_ri', 10, 3);


/** Edit WooCommerce dropdown menu item of shop page
* Options: menu_order, popularity, rating, date, price, price-desc
*/
 
function my_woocommerce_catalog_orderby( $orderby ) {
    unset( $orderby["popularity"] );
    // unset($orderby["rating"]);
    return $orderby;
}
add_filter( "woocommerce_catalog_orderby", "my_woocommerce_catalog_orderby", 20 );


/**
* Force WooCommerce to always show selected variation price
*/
function always_show_variation_prices($show, $parent, $variation) {
    return true;
}
add_filter( 'woocommerce_show_variation_price', 'always_show_variation_prices', 99, 3);

// /**
//  * Custome default order of products in category
//  **/
// add_filter('woocommerce_default_catalog_orderby', 'custom_default_catalog_orderby');

// function custom_default_catalog_orderby() {
//      return 'date'; // Can also use title and price
// }


// Alway at the end of this file
// Remove breadcrumbs 
// add_filter( 'woocommerce_before_main_content', 'remove_breadcrumbs');
// function remove_breadcrumbs() {
//     // if(!is_product()) {
//         remove_action( 'woocommerce_before_main_content','woocommerce_breadcrumb', 20, 0);
//     // }
// }


add_filter( 'wc_add_to_cart_message_html', 'custom_add_to_cart_message' );
 
function custom_add_to_cart_message() {
    global $woocommerce;
    $return_to  = get_permalink(wc_get_page_id('shop'));
    $message    = sprintf('%s <a href="%s" class="">%s</a>', __('Sản phẩm được thêm vào giỏ hàng.', 'woocommerce'), $return_to, __('Nhấn vào đây để mua tiếp.', 'woocommerce') );
    return $message;
}


/**
 * Validate product quantity on when add to cart.
 */
add_filter('woocommerce_add_to_cart_validation', 'validate_quantity_when_add_to_cart', 10, 5);
function validate_quantity_when_add_to_cart( $passed, $product_id, $quantity, $variation_id, $variations ) {
    global $ri_array, $le_array, $woocommerce, $units;

    $dvm_attr = 'attribute_' . DON_VI_MUA_KEY;
    if( $variations && isset( $variations[$dvm_attr] ) ) {
        if ( in_array( $variations[$dvm_attr], $le_array ) && ( $quantity < MIN_QUANTITY_LE) ) {
            // Sets error message.
            wc_add_notice( __( 'Số lượng mua với đơn vị ' . mb_strtoupper( $units[$variations[$dvm_attr]],'UTF-8' ) . ' phải >= ' . MIN_QUANTITY_LE , 'woocommerce' ), 'error' );
            $passed = false;
        } elseif( in_array( $variations[$dvm_attr], $ri_array ) && ( $quantity < MIN_QUANTITY_RI) ) {
            wc_add_notice( __( 'Số lượng mua với đơn vị ' . mb_strtoupper( $units[$variations[$dvm_attr]], 'UTF-8' ) . ' phải >= ' . MIN_QUANTITY_RI, 'woocommerce' ), 'error' );
            $passed = false;
        }
    }

    return $passed;
}


/**
 * Validate product quantity on cart update.
 */
function validate_quantity_on_update_cart( $valid, $cart_item_key, $cart_item, $quantity ) {
    global $ri_array, $le_array, $woocommerce, $units;
    $dvm_attr = 'attribute_' . DON_VI_MUA_KEY;
    if ( isset( $cart_item['variation'][$dvm_attr] ) && in_array( $cart_item['variation'][$dvm_attr], $le_array ) ) {
        $min_quantiy = MIN_QUANTITY_LE;
    } else {
        $min_quantiy = MIN_QUANTITY_RI;
    }

    if( $quantity < $min_quantiy ) {
        wc_add_notice( __( 'Số lượng mua ' .  $cart_item['data']->get_title() . ' với đơn vị ' . mb_strtoupper( $units[$cart_item['variation'][$dvm_attr]], 'UTF-8' ) . ' phải >= ' . $min_quantiy, 'woocommerce' ), 'error' );
        $passed = false;
    }
    return $passed;
    
}
add_filter( 'woocommerce_update_cart_validation', 'validate_quantity_on_update_cart', 10, 4 );


/**
 * Display navigation to next/previous pages when applicable
 */
if ( ! function_exists( 'vecoshop_flatsome_content_nav' ) ) :

function vecoshop_flatsome_content_nav( $nav_id ) {
    global $wp_query, $post;

    // Don't print empty markup on single pages if there's nowhere to navigate.
    if ( is_single() ) {
        $previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
        $next = get_adjacent_post( false, '', false );

        if ( ! $next && ! $previous )
            return;
    }

    // Don't print empty markup in archives if there's only one page.
    if ( $wp_query->max_num_pages < 2 && ( is_home() || is_archive() || is_search() ) )
        return;

    $nav_class = ( is_single() ) ? 'navigation-post' : 'navigation-paging';

    ?>
    <?php if ( is_single() ) : // navigation links for single posts ?>
    <nav role="navigation" id="<?php echo esc_attr( $nav_id ); ?>" class="<?php echo $nav_class; ?>">
    <div class="flex-row next-prev-nav bt bb">
        <div class="flex-col flex-grow nav-prev text-left">
                <?php previous_post_link( '<div class="nav-previous">%link</div>','<span class="hide-for-small">' .get_flatsome_icon('icon-angle-left') . _x( '', 'Previous post link', 'flatsome' ) . '</span> %title', true ); ?>

        </div>
        <div class="flex-col flex-grow nav-next text-right">
                <?php next_post_link( '<div class="nav-next">%link</div>', '%title <span class="hide-for-small">'. get_flatsome_icon('icon-angle-right') . _x( '', 'Next post link', 'flatsome' ) . '</span>', true ); ?>
        </div>
    </div>

    <?php elseif ( $wp_query->max_num_pages > 1 && ( is_home() || is_archive() || is_search() ) ) : // navigation links for home, archive, and search pages ?>

    <div class="flex-row">
        <div class="flex-col flex-grow">
   <?php if ( get_next_posts_link() ) : ?>
        <div class="nav-previous"><?php next_posts_link( __( '<span class="icon-angle-left"></span> Older posts', 'flatsome' ) ); ?></div>
        <?php endif; ?>
        </div>
        <div class="flex-col flex-grow">
          <?php if ( get_previous_posts_link() ) : ?>
             <div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="icon-angle-right"></span>', 'flatsome' ) ); ?></div>
         <?php endif; ?>        </div>
    </div>
    <?php endif; ?>
    </nav><!-- #<?php echo esc_html( $nav_id ); ?> -->

    <?php
}
endif; // flatsome_content_nav



//add_filter('widget_text', 'do_shortcode');

// My Account: remove crowfunding's menu 
add_filter('woocommerce_account_menu_items', 'hl_my_account_menu_items');
function hl_my_account_menu_items($item) {
    unset( $item['dashboard'] );
    unset( $item['bookmarks'] );
    unset( $item['downloads'] );

    return $item;
}

/**
* validate order amount before checkout
*/
add_action( 'woocommerce_checkout_process', 'wc_minimum_order_amount' );
add_action( 'woocommerce_before_cart' , 'wc_minimum_order_amount' );
 
function wc_minimum_order_amount() {
    // Set this variable to specify a minimum order value
    $minimum = 5000000;

    if ( WC()->cart->subtotal < $minimum ) {

        if( is_cart() ) {

            wc_print_notice( 
                sprintf( 'Mỗi đơn hàng bắt buộc trên %s, đơn hàng của bạn là %s.' , 
                    wc_price( $minimum ), 
                    wc_price( WC()->cart->subtotal )
                ), 'error' 
            );

        } else {

            wc_add_notice( 
                sprintf( 'Mỗi đơn hàng bắt buộc trên %s, đơn hàng của bạn là %s.' , 
                    wc_price( $minimum ), 
                    wc_price( WC()->cart->subtotal )
                ), 'error' 
            );

        }
    }

}

/**
 * Ẩn mã bưu chính
 * Ẩn địa chỉ thứ hai
 * Đổi tên Bang / Hạt thành Tỉnh / Thành
 * Đổi tên Tỉnh / Thành phố thành Quận / Huyện
 * 
 * 
 * @hook woocommerce_checkout_fields
 * @param $fields
 * @return mixed
 */
function tp_custom_checkout_fields( $fields ) {
 // Ẩn mã bưu chính
 unset( $fields['postcode'] );
 
 // Ẩn địa chỉ thứ hai
 unset( $fields['address_2'] );
 // unset( $fields['country'] );
 
 // Đổi tên Bang / Hạt thành Tỉnh / Thành
 $fields['state']['label'] = 'Tỉnh / Thành phố *';
 
 // Đổi tên Tỉnh / Thành phố thành Quận / Huyện
 $fields['city']['label'] = 'Quận / Huyện';
 
 
 return $fields;
}
add_filter( 'woocommerce_default_address_fields', 'tp_custom_checkout_fields' );


/**
 * Thay đổi cách hiển thii địa chỉ người dùng 
 * 
 * @hook woocommerce_localisation_address_formats
 * @param $formats
 * @return $formats
 */
add_filter( 'woocommerce_localisation_address_formats', 'woocommerce_custom_address_format', 20 );
function woocommerce_custom_address_format( $formats ) {
    $address_format = "Công ty: {company} \n Tên:  {name} \n Đường: {address_1} \n Quận/Huyện: {city} \n Tỉnh/TP: {state} ";
    $formats[ 'VN' ] = $address_format; 
    return $formats;
}

/**
 * Add Vietnam provinces and cities.
 */
add_filter( 'woocommerce_states', 'vietnam_cities_woocommerce' );
function vietnam_cities_woocommerce( $states ) {
  $states['VN'] = array(
    'CANTHO' => __('Cần Thơ', 'woocommerce') ,
    'HCM' => __('Hồ Chí Minh', 'woocommerce') ,
    'HANOI' => __('Hà Nội', 'woocommerce') ,
    'HAIPHONG' => __('Hải Phòng', 'woocommerce') ,
    'DANANG' => __('Đà Nẵng', 'woocommerce') ,
    'ANGIAG' => __('An Giang', 'woocommerce') ,
    'BRVT' => __('Bà Rịa - Vũng Tàu', 'woocommerce') ,
    'BALIE' => __('Bạc Liêu', 'woocommerce') ,
    'BACKAN' => __('Bắc Kạn', 'woocommerce') ,
    'BACNINH' => __('Bắc Ninh', 'woocommerce') ,
    'BACGIANG' => __('Bắc Giang', 'woocommerce') ,
    'BENTRE' => __('Bến Tre', 'woocommerce') ,
    'BDUONG' => __('Bình Dương', 'woocommerce') ,
    'BDINH' => __('Bình Định', 'woocommerce') ,
    'BPHUOC' => __('Bình Phước', 'woocommerce') ,
    'BTHUAN' => __('Bình Thuận', 'woocommerce'),
    'CAMAU' => __('Cà Mau', 'woocommerce'),
    'DAKLAK' => __('Đak Lak', 'woocommerce'),
    'DAKNONG' => __('Đak Nông', 'woocommerce'),
    'DIENBIEN' => __('Điện Biên', 'woocommerce'),
    'ĐNAI' => __('Đồng Nai', 'woocommerce'),
    'GIALAI' => __('Gia Lai', 'woocommerce'),
    'HGIANG' => __('Hà Giang', 'woocommerce'),
    'HNAM' => __('Hà Nam', 'woocommerce'),
    'HTINH' => __('Hà Tĩnh', 'woocommerce'),
    'HDUONG' => __('Hải Dương', 'woocommerce'),
    'HUGIANG' => __('Hậu Giang', 'woocommerce'),
    'HOABINH' => __('Hòa Bình', 'woocommerce'),
    'HYEN' => __('Hưng Yên', 'woocommerce'),
    'KHOA' => __('Khánh Hòa', 'woocommerce'),
    'KGIANG' => __('Kiên Giang', 'woocommerce'),
    'KTUM' => __('Kom Tum', 'woocommerce'),
    'LCHAU' => __('Lai Châu', 'woocommerce'),
    'LAMDONG' => __('Lâm Đồng', 'woocommerce'),
    'LSON' => __('Lạng Sơn', 'woocommerce'),
    'LCAI' => __('Lào Cai', 'woocommerce'),
    'LAN' => __('Long An', 'woocommerce'),
    'NDINH' => __('Nam Định', 'woocommerce'),
    'NGAN' => __('Nghệ An', 'woocommerce'),
    'NBINH' => __('Ninh Bình', 'woocommerce'),
    'NTHUAN' => __('Ninh Thuận', 'woocommerce'),
    'PTHO' => __('Phú Thọ', 'woocommerce'),
    'PYEN' => __('Phú Yên', 'woocommerce'),
    'QBINH' => __('Quảng Bình', 'woocommerce'),
    'QNAM' => __('Quảng Nam', 'woocommerce'),
    'QNGAI' => __('Quảng Ngãi', 'woocommerce'),
    'QNINH' => __('Quảng Ninh', 'woocommerce'),
    'QTRI' => __('Quảng Trị', 'woocommerce'),
    'STRANG' => __('Sóc Trăng', 'woocommerce'),
    'SLA' => __('Sơn La', 'woocommerce'),
    'TNINH' => __('Tây Ninh', 'woocommerce'),
    'TBINH' => __('Thái Bình', 'woocommerce'),
    'TNGUYEN' => __('Thái Nguyên', 'woocommerce'),
    'THOA' => __('Thanh Hóa', 'woocommerce'),
    'TTHIEN' => __('Thừa Thiên - Huế', 'woocommerce'),
    'TGIANG' => __('Tiền Giang', 'woocommerce'),
    'TVINH' => __('Trà Vinh', 'woocommerce'),
    'TQUANG' => __('Tuyên Quang', 'woocommerce'),
    'VLONG' => __('Vĩnh Long', 'woocommerce'),
    'VPHUC' => __('Vĩnh Phúc', 'woocommerce'),
    'YBAI' => __('Yên Bái', 'woocommerce'),
  );
 
  return $states;
}