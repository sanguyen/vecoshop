<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

// add_action( 'woocommerce_before_my_account','add_charity_dashboard_button', 0 );
add_action( 'init', 'veco_change_avatar_endpoints' );
add_filter( 'query_vars', 'veco_change_avatar_query_vars', 0 );
add_filter('woocommerce_account_menu_items', 'veco_my_account_menu_items');
add_action( 'woocommerce_account_avatar_endpoint',  'veco_change_avatar_dashboard_endpoint_content' );


// Add button Charity Dashboard in my-account dashboard
// function add_charity_dashboard_button() {
//     if ( hl_is_charity() ) {
//         printf( '<a href="%s" title="%s" class="button charity-dashboard-link">%s</a>', esc_url( admin_url() ), esc_attr( __( 'Charity Dashboard', 'hireluv' ) ), __( 'Charity Dashboard', 'hireluv' ) );
//     }
// }

// Rewrite Rules For Woocommerce My Account Page
function veco_change_avatar_endpoints() {
    add_rewrite_endpoint( 'avatar', EP_ROOT | EP_PAGES );
    flush_rewrite_rules();
}


// Query Variable
function veco_change_avatar_query_vars( $vars ) {
    $vars[] = 'avatar';
    return $vars;
}

function veco_my_account_menu_items( $item ) {
    unset( $item['dashboard'] );
    unset( $item['bookmarks'] );
    unset( $item['downloads'] );  
    unset( $item['payment-methods'] );  

    $item['avatar'] = __( 'Hình avatar', 'veco' );

    return $item;
}

// Charity Dashboard
function veco_change_avatar_dashboard_endpoint_content() {
    // echo '<h3>Premium WooCommerce Support</h3><p>Welcome to the WooCommerce support area. As a premium customer, you can submit a ticket should you have any WooCommerce issues with your website, snippets or customization. <i>Please contact your theme/plugin developer for theme/plugin-related support.</i></p>';
    echo do_shortcode( '[avatar_upload]' );
}


?>
