<?php
/**
 * Loop Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $units;

$product_unit = get_min_product_unit( $product );
$ri_price_html = get_ri_price_html( $product );
?>

<?php if ( !empty( $product_unit ) && $price_html = $product->get_price_html() ) : ?>
	<div class="price"><?php echo ucfirst( $units[$product_unit] ) . ': ' . $price_html; ?></div>
<?php endif; ?>

<?php if ( !empty( $ri_price_html ) ): ?> 
	<?php foreach ($ri_price_html as $key_ri => $value_ri) : ?>
		<div class="price"> <?php echo ucfirst( $units[$key_ri] ) . ': ' . $value_ri; ?> </div>
	<?php endforeach; ?>
<?php endif;?>