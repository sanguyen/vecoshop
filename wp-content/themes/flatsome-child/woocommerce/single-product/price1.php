<?php
/**
 * Single Product Price, including microdata for SEO
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $units;

$product_unit = get_post_meta( $product->get_id(), '_unit_of_product', true );
// $enable_ri = get_post_meta( $product->get_id(), '_enable_ri_price', true );
// $number_products = get_post_meta( $product->get_id(), '_number_products_of_ri', true );

// if ( $enable_ri ) {
// 	$price_ri = get_post_meta( $product->get_id(), '_price_of_ri', true );
// }

$classes = array();
if($product->is_on_sale()) $classes[] = 'price-on-sale';
if(!$product->is_in_stock()) $classes[] = 'price-not-in-stock'; 

$ri_price_html = get_ri_price_html( $product );
$dvm_terms = wc_get_product_terms(  $product->get_id(), 'pa_don-vi-mua', array( 'fields' => 'all' ) );

?>
<table class="price-wrapper price-desc-dvm <?php echo implode(' ', $classes); ?>" >
	<tbody>
	<?php if ( !empty( $dvm_terms ) ): ?>
		<?php foreach ( $dvm_terms as $dvm ) { 
	        $dvm_item_name = '_dvm_' . $dvm->slug;
	        // $dvm_item_desc =  get_term_meta( $dvm->term_id, $dvm_item_name, true ); 
	        $dvm_item_desc = get_post_meta(  $product->get_id(), $dvm_item_name, true ); ?>

	        <?php if( $dvm->slug == 'cai' || $dvm->slug == 'bo' ): ?>
				<tr>
					<th class = "price-dvm-label" > <?php echo ucfirst( $dvm->name ); ?> </th>
					<th class = "price-dvm"> <?php echo $product->get_price_html(); ?> </th>
				</tr>
				<tr>
					<td class = "desc-dvm"></td>
					<td class = "desc-dvm"> <?php echo mynl2br( $dvm_item_desc ); ?> </td>
				</tr>
		  	<?php elseif ( !empty( $ri_price_html ) ): ?> 
				<tr>
					<th class = "price-dvm-label"> <?php echo ucfirst( $dvm->name ); ?> </th>
					<th class = "price-dvm"> <?php echo $ri_price_html[$dvm->slug]; ?> </th>
				</tr>
				<tr>
					<td class = "desc-dvm"></td>
					<td class = "desc-dvm"> <?php echo mynl2br( $dvm_item_desc ); ?> </td>
				</tr>

		  	<?php endif;?>
  		<?php } ?>
  	<?php endif ?>
	</tbody>

</table>
